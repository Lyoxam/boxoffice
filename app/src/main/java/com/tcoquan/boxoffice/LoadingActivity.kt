package com.tcoquan.boxoffice

import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.progressindicator.CircularProgressIndicator
import com.tcoquan.data.BuildConfig

abstract class LoadingActivity : AppCompatActivity() {

    abstract var progressBar: CircularProgressIndicator

    fun showLoading() {
        runOnUiThread {
            progressBar.visibility = View.VISIBLE
        }
    }

    fun hideLoading() {
        runOnUiThread {
            progressBar.visibility = View.GONE
        }
    }

    fun displayError(error: Throwable) {
        val errorMessage = if (BuildConfig.DEBUG) {
            error.localizedMessage
        } else {
            getString(R.string.genericError)
        }
        runOnUiThread {
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
            hideLoading()
        }
    }
}