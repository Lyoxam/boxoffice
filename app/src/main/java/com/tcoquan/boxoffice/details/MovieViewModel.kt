package com.tcoquan.boxoffice.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.usecase.movie.GetMovie
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieViewModel
@Inject constructor(
    private val getMovie: GetMovie
) : ViewModel() {

    val error: MutableLiveData<Throwable> = MutableLiveData()
    val movie: MutableLiveData<MovieDetailsInterface> = MutableLiveData()

    fun getMovieInfo(id: String) {
        getMovie(id)
            .subscribe({
                movie.postValue(it)
            }, {
                error.postValue(it)
            })
    }
}