package com.tcoquan.boxoffice.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.progressindicator.CircularProgressIndicator
import com.squareup.picasso.Picasso
import com.tcoquan.boxoffice.LoadingActivity
import com.tcoquan.boxoffice.R
import com.tcoquan.boxoffice.databinding.ActivityMovieBinding
import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.model.MovieInterface
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class MovieActivity : LoadingActivity() {

    companion object {
        private const val EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID"

        fun createIntent(context: Context, movie: MovieInterface): Intent {
            val intent = Intent(context, MovieActivity::class.java)
            intent.putExtra(EXTRA_MOVIE_ID, movie.id)
            return intent
        }
    }

    private lateinit var binding: ActivityMovieBinding
    private lateinit var viewModel: MovieViewModel

    override var progressBar: CircularProgressIndicator
        get() = binding.progressCircular
        set(_) {}

    private val releaseDateFormat = SimpleDateFormat("dd MMM yyyy", Locale.UK)

    //region Android
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = getString(R.string.movieDetailsTitle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel = ViewModelProvider(this).get(MovieViewModel::class.java)
        viewModel.error.observe(this, { displayError(it) })
        viewModel.movie.observe(this, { displayMovie(it) })
        showLoading()
        viewModel.getMovieInfo(intent.getStringExtra(EXTRA_MOVIE_ID)!!)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
    //endregion

    private fun displayMovie(movie: MovieDetailsInterface) {
        runOnUiThread {
            binding.txtTitle.text = movie.title
            Picasso.get().load(movie.icon).into(binding.imgPoster)
            if (movie.released != null) {
                binding.txtReleaseDate.text = getString(R.string.movieDetailsRelease, releaseDateFormat.format(movie.released!!))
            } else {
                binding.txtReleaseDate.visibility = View.GONE
            }
            binding.txtProduction.text = getString(R.string.movieDetailsProduction, movie.production)
            binding.txtBoxOffice.text = getString(R.string.movieDetailsBoxOffice, movie.boxOffice)

            if (movie.score != -1) {
                binding.ratingBarCritics.rating = movie.score / 100f * 5
            } else {
                binding.ratingBarCritics.visibility = View.GONE
                binding.txtCritics.visibility = View.GONE
            }

            if (movie.rating != -1f) {
                binding.ratingBarAudience.rating = movie.rating / 10 * 5
            } else {
                binding.ratingBarAudience.visibility = View.GONE
                binding.txtAudience.visibility = View.GONE
            }
            binding.txtSynopsisValue.text = movie.plot
            binding.txtCastingValue.text = movie.actors
            binding.txtGenreValue.text = movie.genre
            hideLoading()
        }
    }
}