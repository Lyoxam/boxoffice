package com.tcoquan.boxoffice.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tcoquan.domain.model.MovieInterface
import com.tcoquan.domain.usecase.movie.SearchMovies
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel
@Inject constructor(
    private val searchMovies: SearchMovies
) : ViewModel() {

    val error: MutableLiveData<Throwable> = MutableLiveData()
    val movies: MutableLiveData<MutableList<MovieInterface>> = MutableLiveData()

    private var lastQuery: String? = null
    private var lastPage: Int = 1

    fun repeatLastSearch() {
        if (lastQuery != null) {
            search(lastQuery!!)
        } else {
            movies.postValue(mutableListOf())
        }
    }

    fun search(query: String) {
        lastQuery = query
        lastPage = 1
        searchMovies(query)
            .subscribe({
                movies.postValue(it.toMutableList())
            }, {
                error.postValue(it)
            })
    }

    fun hasPreviousQuery(): Boolean {
        return lastQuery != null
    }

    fun searchNext() {
        if (lastQuery != null) {
            lastPage++
            searchMovies(lastQuery!!, lastPage)
                .subscribe({
                    val tempList = movies.value
                    tempList?.addAll(it)
                    movies.postValue(tempList)
                }, {
                    error.postValue(it)
                })
        }
    }
}