package com.tcoquan.boxoffice.list

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.tcoquan.boxoffice.R
import com.tcoquan.boxoffice.databinding.ItemMovieBinding
import com.tcoquan.boxoffice.details.MovieActivity
import com.tcoquan.domain.model.MovieInterface


class MovieAdapter() : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    private val data: MutableList<MovieInterface> = mutableListOf()

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(data[position], position % 2 == 0)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    fun setData(movies: List<MovieInterface>) {
        data.clear()
        data.addAll(movies)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindTo(movie: MovieInterface, isEven: Boolean) {
            if (!isEven) {
                binding.mainLayout.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.movie_even_item))
            } else {
                val typedValue = TypedValue()
                itemView.context.theme.resolveAttribute(R.attr.colorSurface, typedValue, true)
                binding.mainLayout.setBackgroundColor(typedValue.data)
            }

            binding.txtMovieName.text = movie.title
            binding.txtMovieYear.text = movie.year
            Picasso.get().load(movie.icon).resize(80, 80).centerCrop().into(binding.imgMovieIcon)

            binding.mainLayout.setOnClickListener {
                val intent = MovieActivity.createIntent(itemView.context, movie)
                itemView.context.startActivity(intent)
            }
        }
    }
}