package com.tcoquan.boxoffice.list

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.progressindicator.CircularProgressIndicator
import com.tcoquan.boxoffice.LoadingActivity
import com.tcoquan.boxoffice.R
import com.tcoquan.boxoffice.databinding.ActivityMoviesBinding
import com.tcoquan.domain.model.MovieInterface
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoviesActivity : LoadingActivity(), SearchView.OnQueryTextListener {

    private lateinit var binding: ActivityMoviesBinding
    private lateinit var viewModel: MoviesViewModel

    private lateinit var movieAdapter: MovieAdapter

    override var progressBar: CircularProgressIndicator
        get() = binding.progressCircular
        set(_) {}

    private var searching = false

    //region Android
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMoviesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MoviesViewModel::class.java)
        viewModel.error.observe(this, { displayError(it) })
        viewModel.movies.observe(this, { displayMovies(it) })

        binding.swipeLayout.setOnRefreshListener {
            if (!searching) {
                searching = true
                viewModel.repeatLastSearch()
            }
        }

        movieAdapter = MovieAdapter()
        binding.rvMovies.adapter = movieAdapter
        binding.rvMovies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1)) {
                    if (!searching && viewModel.hasPreviousQuery()) {
                        searching = true
                        showLoading()
                        viewModel.searchNext()
                    }
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)

        val searchItem: MenuItem? = menu.findItem(R.id.menu_search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView: SearchView = searchItem?.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(this)

        return super.onCreateOptionsMenu(menu)
    }
    //endregion

    //region Listener
    override fun onQueryTextChange(newText: String): Boolean {
        return true
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        if (!searching) {
            searching = true
            showLoading()
            viewModel.search(query)
        }
        return true
    }
    //endregion

    private fun displayMovies(movies: List<MovieInterface>) {
        runOnUiThread {
            if (movies.isEmpty()) {
                binding.txtEmptyState.visibility = View.VISIBLE
                Toast.makeText(baseContext, R.string.moviesNoResult, Toast.LENGTH_LONG).show()
            } else {
                binding.txtEmptyState.visibility = View.GONE
                if (movieAdapter.itemCount == movies.count()) {
                    Toast.makeText(baseContext, R.string.moviesNoMoreResult, Toast.LENGTH_LONG).show()
                }
            }
            movieAdapter.setData(movies)
            binding.swipeLayout.isRefreshing = false
            searching = false
            hideLoading()
        }
    }
}