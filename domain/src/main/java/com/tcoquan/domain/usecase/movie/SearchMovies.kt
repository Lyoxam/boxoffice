package com.tcoquan.domain.usecase.movie

import com.tcoquan.domain.model.MovieInterface
import io.reactivex.rxjava3.core.Maybe

interface SearchMovies {
    operator fun invoke(searchValue: String, page: Int = 1): Maybe<List<MovieInterface>>
}