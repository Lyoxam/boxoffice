package com.tcoquan.domain.usecase.movie

import com.tcoquan.domain.model.MovieDetailsInterface
import io.reactivex.rxjava3.core.Single

interface GetMovie {
    operator fun invoke(id: String): Single<MovieDetailsInterface>
}