package com.tcoquan.domain.repository

import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.model.MovieInterface
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single

interface MovieRepository {
    fun searchMovies(searchValue: String, page: Int = 1): Maybe<List<MovieInterface>>
    fun get(id: String): Single<MovieDetailsInterface>
}