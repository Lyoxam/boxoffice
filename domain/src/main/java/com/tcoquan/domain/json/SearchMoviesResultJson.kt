package com.tcoquan.domain.json

import com.google.gson.annotations.SerializedName

data class SearchMoviesResultJson(
    @SerializedName("Response") override val responseIsOk: Boolean,
    @SerializedName("Error") override val errorMessage: String?,

    @SerializedName("Search") val movies: List<MovieJson>?
) : BaseJson