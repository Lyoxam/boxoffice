package com.tcoquan.domain.json

import java.io.Serializable

interface BaseJson : Serializable {
    val responseIsOk: Boolean
    val errorMessage: String?
}