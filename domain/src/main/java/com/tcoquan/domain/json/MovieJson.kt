package com.tcoquan.domain.json

import com.google.gson.annotations.SerializedName

data class MovieJson(
    @SerializedName("Response") override val responseIsOk: Boolean,
    @SerializedName("Error") override val errorMessage: String?,

    @SerializedName("imdbID") val id: String,

    @SerializedName("Title") val title: String,
    @SerializedName("Type") val type: String,
    @SerializedName("Poster") val poster: String,
    @SerializedName("Year") val year: String,

    @SerializedName("Rated") val rated: String?,
    @SerializedName("Released") val released: String?,
    @SerializedName("Runtime") val runtime: String?,
    @SerializedName("Genre") val genre: String?,
    @SerializedName("Director") val director: String?,
    @SerializedName("Writer") val writer: String?,
    @SerializedName("Actors") val actors: String?,
    @SerializedName("Plot") val plot: String?,
    @SerializedName("Language") val language: String?,
    @SerializedName("Country") val country: String?,
    @SerializedName("Awards") val awards: String?,
    @SerializedName("Metascore") val score: String?,
    @SerializedName("imdbRating") val rating: String?,
    @SerializedName("imdbVotes") val nbVotes: String?,
    @SerializedName("DVD") val dvd: String?,
    @SerializedName("BoxOffice") val boxOffice: String?,
    @SerializedName("Production") val production: String?,
    @SerializedName("Website") val website: String?,
) : BaseJson