package com.tcoquan.domain.model

import java.util.*

interface MovieDetailsInterface : MovieInterface {
    var actors: String
    var boxOffice: String
    var genre: String
    var plot: String
    var production: String
    var rating: Float
    var released: Date?
    var score: Int
}