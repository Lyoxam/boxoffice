package com.tcoquan.domain.model

interface MovieInterface {
    var id: String
    var title: String
    var icon: String
    var year: String
    var type: MovieType
}

enum class MovieType(var type: String) {
    MOVIE("movie"),
    OTHER("");

    override fun toString(): String {
        return type
    }

    companion object {
        fun fromString(type: String): MovieType {
            return when (type) {
                MOVIE.type -> MOVIE
                else -> OTHER
            }
        }
    }
}