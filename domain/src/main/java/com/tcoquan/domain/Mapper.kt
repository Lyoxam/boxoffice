package com.tcoquan.domain

import java.io.Serializable

interface Mapper<J : Serializable, M> {
    fun convert(json: J): M
}