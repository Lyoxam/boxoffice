object Versions {
    const val minSdk = 21
    const val targetSdk = 29
    const val compileSdk = 29

    const val versionCode = 10000
    const val versionName = "1.0.0"

    const val buildTools = "4.1.3"
    const val kotlin = "1.4.31"

    const val hilt = "2.33-beta"
    const val picasso = "2.71828"
    const val retrofit = "2.9.0"
    const val rxJava3 = "3.0.11"
    const val stetho = "1.5.1"

    //Android
    const val androidXCore = "1.3.2"
    const val androidXAppCompat = "1.2.0"
    const val androidXConstraintLayout = "2.0.4"
    const val androidXSwipeRefreshLayout = "1.1.0"
    const val androidMaterialDesign = "1.3.0"

    //Test
    const val espressoCore = "3.3.0"
    const val junit = "4.13.2"
    const val junitAndroidX = "1.1.2"
    const val mockito = "2.2.11"
}