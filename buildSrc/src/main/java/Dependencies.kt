object Dependencies {

    const val buildTools = "com.android.tools.build:gradle:${Versions.buildTools}"

    //Kotlin
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val kotlinStandardLibrary = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"

    //Android
    const val androidXCore = "androidx.core:core-ktx:${Versions.androidXCore}"
    const val androidXAppCompat = "androidx.appcompat:appcompat:${Versions.androidXAppCompat}"
    const val androidXConstraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.androidXConstraintLayout}"
    const val androidXSwipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.androidXSwipeRefreshLayout}"
    const val androidMaterialDesign = "com.google.android.material:material:${Versions.androidMaterialDesign}"

    //Hilt
    const val hiltPlugin = "com.google.dagger:hilt-android-gradle-plugin:${Versions.hilt}"
    const val hilt = "com.google.dagger:hilt-android:${Versions.hilt}"
    const val hiltCompiler = "com.google.dagger:hilt-compiler:${Versions.hilt}"

    //Picasso
    const val picasso = "com.squareup.picasso:picasso:${Versions.picasso}"

    //Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val gson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val rxJava3Adapter = "com.squareup.retrofit2:adapter-rxjava3:${Versions.retrofit}"

    //Rx
    const val rxJava3 = "io.reactivex.rxjava3:rxjava:${Versions.rxJava3}"

    //Stetho
    const val stetho = "com.facebook.stetho:stetho-okhttp3:${Versions.stetho}"

    //Test
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"
    const val junit = "junit:junit:${Versions.junit}"
    const val junitAndroidX = "androidx.test.ext:junit:${Versions.junitAndroidX}"
    const val junitKotlin = "org.jetbrains.kotlin:kotlin-test-junit:${Versions.kotlin}"
    const val mockito = "org.mockito.kotlin:mockito-kotlin:${Versions.mockito}"
}