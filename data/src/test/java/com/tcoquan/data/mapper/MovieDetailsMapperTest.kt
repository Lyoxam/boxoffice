package com.tcoquan.data.mapper

import com.tcoquan.data.MockValues
import com.tcoquan.data.MockValues.Companion.classicActors
import com.tcoquan.data.MockValues.Companion.classicAwards
import com.tcoquan.data.MockValues.Companion.classicBoxOffice
import com.tcoquan.data.MockValues.Companion.classicCountry
import com.tcoquan.data.MockValues.Companion.classicDirector
import com.tcoquan.data.MockValues.Companion.classicDvd
import com.tcoquan.data.MockValues.Companion.classicGenre
import com.tcoquan.data.MockValues.Companion.classicId
import com.tcoquan.data.MockValues.Companion.classicLanguage
import com.tcoquan.data.MockValues.Companion.classicNbVotes
import com.tcoquan.data.MockValues.Companion.classicPlot
import com.tcoquan.data.MockValues.Companion.classicPoster
import com.tcoquan.data.MockValues.Companion.classicProduction
import com.tcoquan.data.MockValues.Companion.classicRated
import com.tcoquan.data.MockValues.Companion.classicRating
import com.tcoquan.data.MockValues.Companion.classicRatingString
import com.tcoquan.data.MockValues.Companion.classicReleasedString
import com.tcoquan.data.MockValues.Companion.classicRuntime
import com.tcoquan.data.MockValues.Companion.classicScore
import com.tcoquan.data.MockValues.Companion.classicScoreString
import com.tcoquan.data.MockValues.Companion.classicTitle
import com.tcoquan.data.MockValues.Companion.classicType
import com.tcoquan.data.MockValues.Companion.classicWebsite
import com.tcoquan.data.MockValues.Companion.classicWriter
import com.tcoquan.data.MockValues.Companion.classicYear
import com.tcoquan.data.model.MovieDetails
import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.model.MovieType
import org.junit.Before
import org.junit.Test
import java.util.*
import kotlin.test.assertEquals

class MovieDetailsMapperTest {

    lateinit var mapper: MovieDetailsMapper

    private lateinit var classicReleased: Date

    @Before
    fun setUp() {
        mapper = MovieDetailsMapper()

        val instance = Calendar.getInstance()
        instance.set(2000, 0, 1, 0, 0, 0)
        instance.set(Calendar.MILLISECOND, 0)
        classicReleased = Date(instance.timeInMillis)
    }

    @Test
    fun convertClassicMovie() {
        val src = MockValues.movieJsonClassic
        val result = mapper.convert(src)
        assertEquals(result, MockValues.movieDetailsClassic)
    }

    @Test
    fun convertMovieWithoutDetails() {
        val src = MockValues.movieJsonEmpty
        val result = mapper.convert(src)
        assertEquals(result, MockValues.movieDetailsEmpty)
    }

    @Test
    fun convertMovieWithWrongNumberValues() {
        val src = MovieJson(
            true, "", classicId, classicTitle, classicType, classicPoster, classicYear, classicRated, classicReleasedString,
            classicRuntime, classicGenre, classicDirector, classicWriter, classicActors, classicPlot, classicLanguage, classicCountry, classicAwards,
            "wrong", "wrong", classicNbVotes, classicDvd, classicBoxOffice, classicProduction, classicWebsite
        )

        val convert = MovieDetails(
            classicId, classicTitle, classicPoster, MovieType.MOVIE, classicYear, classicActors, classicBoxOffice, classicGenre, classicPlot,
            classicProduction, -1f, classicReleased, -1
        )

        val result = mapper.convert(src)

        assertEquals(result, convert)
    }

    @Test
    fun convertMovieWithWrongDateValues() {
        val src = MovieJson(
            true, "", classicId, classicTitle, classicType, classicPoster, classicYear, classicRated, "wrong",
            classicRuntime, classicGenre, classicDirector, classicWriter, classicActors, classicPlot, classicLanguage, classicCountry, classicAwards,
            classicScoreString, classicRatingString, classicNbVotes, classicDvd, classicBoxOffice, classicProduction, classicWebsite
        )

        val convert = MovieDetails(
            classicId, classicTitle, classicPoster, MovieType.MOVIE, classicYear, classicActors, classicBoxOffice, classicGenre, classicPlot,
            classicProduction, classicRating, null, classicScore
        )

        val result = mapper.convert(src)

        assertEquals(result, convert)
    }
}