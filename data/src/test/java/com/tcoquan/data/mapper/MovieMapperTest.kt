package com.tcoquan.data.mapper

import com.tcoquan.data.MockValues
import com.tcoquan.data.MockValues.Companion.classicId
import com.tcoquan.data.MockValues.Companion.classicPoster
import com.tcoquan.data.MockValues.Companion.classicTitle
import com.tcoquan.data.MockValues.Companion.classicYear
import com.tcoquan.data.model.Movie
import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.model.MovieType
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class MovieMapperTest {

    lateinit var mapper: MovieMapper

    @Before
    fun setUp() {
        mapper = MovieMapper()
    }

    @Test
    fun convertClassicMovie() {
        val src = MockValues.movieJsonClassic
        val convert = MockValues.movieClassic
        val result = mapper.convert(src)
        assertEquals(result, convert)
    }

    @Test
    fun convertMovieWithWrongType() {
        val src = MovieJson(
            true, "", classicId, classicTitle, "wrong", classicPoster, classicYear, null,
            null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null
        )

        val convert = Movie(classicId, classicTitle, classicPoster, MovieType.OTHER, classicYear)
        val result = mapper.convert(src)
        assertEquals(result, convert)
    }
}