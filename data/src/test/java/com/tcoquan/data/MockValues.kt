package com.tcoquan.data

import com.tcoquan.data.model.Movie
import com.tcoquan.data.model.MovieDetails
import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.model.MovieType
import java.util.*

class MockValues {

    companion object {
        const val classicId = "id"
        const val classicTitle = "title"
        const val classicType = "movie"
        const val classicPoster = "poster"
        const val classicYear = "year"
        const val classicRated = "rated"
        const val classicReleasedString = "01 Jan 2000"
        const val classicRuntime = "runtime"
        const val classicGenre = "genre"
        const val classicDirector = "director"
        const val classicWriter = "writer"
        const val classicActors = "actors"
        const val classicPlot = "plot"
        const val classicLanguage = "language"
        const val classicCountry = "country"
        const val classicAwards = "awards"
        const val classicScoreString = "1"
        const val classicScore = 1
        const val classicRatingString = "1.0"
        const val classicRating = 1f
        const val classicNbVotes = "nbVotes"
        const val classicDvd = "dvd"
        const val classicBoxOffice = "boxOffice"
        const val classicProduction = "production"
        const val classicWebsite = "website"

        val movieJsonClassic = MovieJson(
            true, "", classicId, classicTitle, classicType, classicPoster, classicYear, classicRated, classicReleasedString,
            classicRuntime, classicGenre, classicDirector, classicWriter, classicActors, classicPlot, classicLanguage, classicCountry, classicAwards,
            classicScoreString, classicRatingString, classicNbVotes, classicDvd, classicBoxOffice, classicProduction, classicWebsite
        )

        val movieJsonEmpty = MovieJson(
            true, "", classicId, classicTitle, classicType, classicPoster, classicYear, null,
            null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null
        )

        val movieJsonError = MovieJson(
            false, "error", "", "", "", "", "", null,
            null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null
        )

        val movieClassic = Movie(classicId, classicTitle, classicPoster, MovieType.MOVIE, classicYear)

        val movieDetailsClassic = MovieDetails(
            classicId, classicTitle, classicPoster, MovieType.MOVIE, classicYear, classicActors, classicBoxOffice, classicGenre, classicPlot,
            classicProduction, classicRating, initClassicReleased(), classicScore
        )

        val movieDetailsEmpty = MovieDetails(
            classicId, classicTitle, classicPoster, MovieType.MOVIE, classicYear, "", "",
            "", "", "", -1f, null, -1
        )

        private fun initClassicReleased(): Date {
            val instance = Calendar.getInstance()
            instance.set(2000, 0, 1, 0, 0, 0)
            instance.set(Calendar.MILLISECOND, 0)
            return Date(instance.timeInMillis)
        }
    }
}
