package com.tcoquan.data.repository

import com.tcoquan.data.MockValues.Companion.classicId
import com.tcoquan.data.MockValues.Companion.classicTitle
import com.tcoquan.data.MockValues.Companion.movieDetailsClassic
import com.tcoquan.data.MockValues.Companion.movieDetailsEmpty
import com.tcoquan.data.MockValues.Companion.movieJsonClassic
import com.tcoquan.data.MockValues.Companion.movieJsonEmpty
import com.tcoquan.data.MockValues.Companion.movieJsonError
import com.tcoquan.data.api.UserApi
import com.tcoquan.domain.Mapper
import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.json.SearchMoviesResultJson
import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.model.MovieInterface
import com.tcoquan.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever


class MovieRepositoryImplTest {

    lateinit var movieRepository: MovieRepository

    @Mock
    lateinit var userApi: UserApi

    @Mock
    lateinit var mapperMovie: Mapper<MovieJson, MovieInterface>

    @Mock
    lateinit var mapperMovieDetails: Mapper<MovieJson, MovieDetailsInterface>

    private val testListObserver = TestObserver<List<MovieInterface>>()
    private val testObserver = TestObserver<MovieInterface>()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        val classicMovies = mutableListOf<MovieJson>()
        classicMovies.add(movieJsonClassic)
        val userApiSearchClassicMock = Observable.just(SearchMoviesResultJson(true, "", classicMovies.toList()))
        whenever(userApi.searchMovies(classicTitle)).thenReturn(userApiSearchClassicMock)
        val userApiSearchEmptyMock = Observable.just(SearchMoviesResultJson(true, "", emptyList()))
        whenever(userApi.searchMovies("empty")).thenReturn(userApiSearchEmptyMock)
        val userApiSearchErrorMock = Observable.just(SearchMoviesResultJson(false, "error", null))
        whenever(userApi.searchMovies("error")).thenReturn(userApiSearchErrorMock)

        whenever(userApi.getMovieById(classicId)).thenReturn(Observable.just(movieJsonClassic))
        whenever(userApi.getMovieById("empty")).thenReturn(Observable.just(movieJsonEmpty))
        whenever(userApi.getMovieById("error")).thenReturn(Observable.just(movieJsonError))
        whenever(mapperMovie.convert(movieJsonClassic)).thenReturn(movieDetailsClassic)
        whenever(mapperMovieDetails.convert(movieJsonClassic)).thenReturn(movieDetailsClassic)
        whenever(mapperMovieDetails.convert(movieJsonEmpty)).thenReturn(movieDetailsEmpty)

        movieRepository = MovieRepositoryImpl(userApi, mapperMovie, mapperMovieDetails)
    }

    @Test
    fun searchMovies() {
        movieRepository
            .searchMovies(classicTitle)
            .subscribe(testListObserver)

        testListObserver.assertNoErrors()
        testListObserver.assertComplete()
        testListObserver.assertValue {
            it.contains(movieDetailsClassic) && it.size == 1
        }
    }

    @Test
    fun searchNoResult() {
        movieRepository
            .searchMovies("empty")
            .subscribe(testListObserver)

        testListObserver.assertNoErrors()
        testListObserver.assertComplete()
        testListObserver.assertValue(emptyList())
    }

    @Test
    fun searchError() {
        movieRepository
            .searchMovies("error")
            .subscribe(testListObserver)

        testListObserver.assertError(RuntimeException::class.java)
        testListObserver.assertNotComplete()
        testListObserver.assertNoValues()
    }

    @Test
    fun getMovie() {
        movieRepository
            .get(classicId)
            .subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue(movieDetailsClassic)
    }

    @Test
    fun getNoResult() {
        movieRepository
            .get("empty")
            .subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue(movieDetailsEmpty)
    }

    @Test
    fun getError() {
        movieRepository
            .get("error")
            .subscribe(testObserver)

        testObserver.assertError(RuntimeException::class.java)
        testObserver.assertNotComplete()
        testObserver.assertNoValues()
    }
}