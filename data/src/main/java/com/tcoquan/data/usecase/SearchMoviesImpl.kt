package com.tcoquan.data.usecase

import com.tcoquan.domain.model.MovieInterface
import com.tcoquan.domain.repository.MovieRepository
import com.tcoquan.domain.usecase.movie.SearchMovies
import io.reactivex.rxjava3.core.Maybe
import javax.inject.Inject

class SearchMoviesImpl
@Inject constructor(
    private val movieRepository: MovieRepository
) : SearchMovies {
    override fun invoke(searchValue: String, page: Int): Maybe<List<MovieInterface>> {
        return movieRepository.searchMovies(searchValue, page)
    }
}