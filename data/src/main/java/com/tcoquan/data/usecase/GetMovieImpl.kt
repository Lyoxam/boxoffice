package com.tcoquan.data.usecase

import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.repository.MovieRepository
import com.tcoquan.domain.usecase.movie.GetMovie
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetMovieImpl
@Inject constructor(
    private val movieRepository: MovieRepository
) : GetMovie {
    override fun invoke(id: String): Single<MovieDetailsInterface> {
        return movieRepository.get(id)
    }
}