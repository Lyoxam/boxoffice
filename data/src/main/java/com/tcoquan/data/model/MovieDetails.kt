package com.tcoquan.data.model

import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.model.MovieType
import java.util.*

class MovieDetails(
    override var id: String,

    override var title: String,
    override var icon: String,
    override var type: MovieType,
    override var year: String,

    override var actors: String,
    override var boxOffice: String,
    override var genre: String,
    override var plot: String,
    override var production: String,
    override var rating: Float,
    override var released: Date?,
    override var score: Int
) : MovieDetailsInterface {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MovieDetails

        if (id != other.id) return false
        if (title != other.title) return false
        if (icon != other.icon) return false
        if (type != other.type) return false
        if (year != other.year) return false
        if (actors != other.actors) return false
        if (boxOffice != other.boxOffice) return false
        if (genre != other.genre) return false
        if (plot != other.plot) return false
        if (production != other.production) return false
        if (rating != other.rating) return false
        if (released != other.released) return false
        if (score != other.score) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + icon.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + year.hashCode()
        result = 31 * result + actors.hashCode()
        result = 31 * result + boxOffice.hashCode()
        result = 31 * result + genre.hashCode()
        result = 31 * result + plot.hashCode()
        result = 31 * result + production.hashCode()
        result = 31 * result + rating.hashCode()
        result = 31 * result + (released?.hashCode() ?: 0)
        result = 31 * result + score
        return result
    }
}