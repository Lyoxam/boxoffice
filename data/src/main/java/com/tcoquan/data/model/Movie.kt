package com.tcoquan.data.model

import com.tcoquan.domain.model.MovieInterface
import com.tcoquan.domain.model.MovieType

class Movie(
    override var id: String,

    override var title: String,
    override var icon: String,
    override var type: MovieType,
    override var year: String
) : MovieInterface {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Movie

        if (id != other.id) return false
        if (title != other.title) return false
        if (icon != other.icon) return false
        if (type != other.type) return false
        if (year != other.year) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + icon.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + year.hashCode()
        return result
    }
}