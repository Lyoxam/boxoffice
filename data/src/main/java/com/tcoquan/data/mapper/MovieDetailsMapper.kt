package com.tcoquan.data.mapper

import com.tcoquan.data.model.MovieDetails
import com.tcoquan.domain.Mapper
import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.model.MovieType
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Singleton

@Singleton
class MovieDetailsMapper : Mapper<MovieJson, MovieDetailsInterface> {

    private val movieReleasedDateFormatter = SimpleDateFormat("dd MMM yyyy", Locale.UK)

    override fun convert(json: MovieJson): MovieDetailsInterface {
        val rating = try {
            if (json.rating != null) json.rating!!.toFloat() else -1f
        } catch (exception: NumberFormatException) {
            -1f
        }

        val score = try {
            if (json.score != null) json.score!!.toInt() else -1
        } catch (exception: NumberFormatException) {
            -1
        }

        val actors = if (json.actors != null) json.actors!! else ""
        val boxOffice = if (json.boxOffice != null) json.boxOffice!! else ""
        val genre = if (json.genre != null) json.genre!! else ""
        val plot = if (json.plot != null) json.plot!! else ""
        val production = if (json.production != null) json.production!! else ""
        val released = try {
            if (json.released != null) movieReleasedDateFormatter.parse(json.released!!) else null
        } catch (exception: ParseException) {
            null
        }

        return MovieDetails(
            json.id,
            json.title,
            json.poster,
            MovieType.fromString(json.type),
            json.year,
            actors,
            boxOffice,
            genre,
            plot,
            production,
            rating,
            released,
            score
        )
    }
}