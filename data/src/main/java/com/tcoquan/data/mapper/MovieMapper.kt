package com.tcoquan.data.mapper

import com.tcoquan.data.model.Movie
import com.tcoquan.domain.Mapper
import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.model.MovieInterface
import com.tcoquan.domain.model.MovieType

class MovieMapper : Mapper<MovieJson, MovieInterface> {
    override fun convert(json: MovieJson): MovieInterface {
        return Movie(
            json.id,
            json.title,
            json.poster,
            MovieType.fromString(json.type),
            json.year
        )
    }
}