package com.tcoquan.data.module

import com.tcoquan.data.api.UserApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module(includes = [NetworkModule::class])
@InstallIn(SingletonComponent::class)
class ApiModule {

    @Provides
    fun provideUserWebService(retrofit: Retrofit): UserApi {
        return retrofit.create(UserApi::class.java)
    }
}