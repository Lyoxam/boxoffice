package com.tcoquan.data.module

import com.tcoquan.data.mapper.MovieDetailsMapper
import com.tcoquan.data.mapper.MovieMapper
import com.tcoquan.domain.Mapper
import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.model.MovieInterface
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(includes = [NetworkModule::class])
@InstallIn(SingletonComponent::class)
class MapperModule {

    @Provides
    fun provideMovieMapper(): Mapper<MovieJson, MovieInterface> {
        return MovieMapper()
    }

    @Provides
    fun provideMovieDetailsMapper(): Mapper<MovieJson, MovieDetailsInterface> {
        return MovieDetailsMapper()
    }
}