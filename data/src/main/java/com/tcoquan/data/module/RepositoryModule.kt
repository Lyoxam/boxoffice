package com.tcoquan.data.module

import com.tcoquan.data.api.UserApi
import com.tcoquan.data.repository.MovieRepositoryImpl
import com.tcoquan.domain.Mapper
import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.model.MovieInterface
import com.tcoquan.domain.repository.MovieRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    fun provideMovieRepository(
        userApi: UserApi,
        mapper: Mapper<MovieJson, MovieInterface>,
        mapperDetails: Mapper<MovieJson, MovieDetailsInterface>
    ): MovieRepository {
        return MovieRepositoryImpl(userApi, mapper, mapperDetails)
    }
}