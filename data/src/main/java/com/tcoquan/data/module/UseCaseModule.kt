package com.tcoquan.data.module

import com.tcoquan.data.usecase.GetMovieImpl
import com.tcoquan.data.usecase.SearchMoviesImpl
import com.tcoquan.domain.repository.MovieRepository
import com.tcoquan.domain.usecase.movie.GetMovie
import com.tcoquan.domain.usecase.movie.SearchMovies
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    fun provideSearchMovies(movieRepository: MovieRepository): SearchMovies {
        return SearchMoviesImpl(movieRepository)
    }

    @Provides
    fun provideGetMovie(movieRepository: MovieRepository): GetMovie {
        return GetMovieImpl(movieRepository)
    }
}