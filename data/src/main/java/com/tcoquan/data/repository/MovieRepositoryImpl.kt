package com.tcoquan.data.repository

import com.tcoquan.data.api.UserApi
import com.tcoquan.domain.Mapper
import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.model.MovieDetailsInterface
import com.tcoquan.domain.model.MovieInterface
import com.tcoquan.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepositoryImpl
@Inject constructor(
    private val userApi: UserApi,
    private val mapper: Mapper<MovieJson, MovieInterface>,
    private val mapperDetails: Mapper<MovieJson, MovieDetailsInterface>
) : MovieRepository {
    override fun searchMovies(searchValue: String, page: Int): Maybe<List<MovieInterface>> {
        return userApi
            .searchMovies(searchValue, page)
            .map {
                if (!it.errorMessage.isNullOrEmpty()) {
                    throw RuntimeException(it.errorMessage)
                }
                val movies = mutableListOf<MovieInterface>()
                it.movies?.forEach { movieJson ->
                    movies.add(mapper.convert(movieJson))
                }
                movies.toList()
            }.singleElement()
    }

    override fun get(id: String): Single<MovieDetailsInterface> {
        return userApi
            .getMovieById(id)
            .map {
                if (!it.errorMessage.isNullOrEmpty()) {
                    throw RuntimeException(it.errorMessage)
                }
                mapperDetails.convert(it)
            }
            .singleOrError()
    }
}