package com.tcoquan.data.api

import com.tcoquan.domain.json.MovieJson
import com.tcoquan.domain.json.SearchMoviesResultJson
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApi {
    @GET(".")
    fun searchMovies(
        @Query("s") searchValue: String,
        @Query("page") page: Int = 1
    ): Observable<SearchMoviesResultJson>

    @GET(".")
    fun getMovieById(
        @Query("i") movieId: String,
        @Query("plot") plot: String = "full"
    ): Observable<MovieJson>
}